steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE shop_apparels (
            id SERIAL PRIMARY KEY NOT NULL,
            shop_id INTEGER REFERENCES shops (id) NOT NULL,
            apparel_id INTEGER REFERENCES apparels (id) NOT NULL,
            quantity INTEGER NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE shop_apparels;
        """
    ],
]
