async def seed():
    from . import seed_data
    import os

    db_url = os.environ["DATABASE_URL"]

    await seed_data(db_url)

if __name__ == "__main__":
    from asyncio import run

    run(seed())
