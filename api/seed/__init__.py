from importlib import import_module
import os.path
from pathlib import Path

from psycopg import AsyncConnection


async def read_seed_files(dir: str):
    queries = []
    files = sorted(
        [
            file
            for file in Path(dir).iterdir()
            if not str(file.name).startswith("__")
        ]
    )
    for file in files:
        if file.suffix == ".py":
            m = import_module(f".{str(file.stem)}", package=__package__)
            queries.append(m.query)
    return queries

async def seed_data(db_url, dir=os.path.dirname(__file__)):
    queries = await read_seed_files(dir)
    async with await AsyncConnection.connect(db_url) as conn:
        async with conn.cursor() as db:
            for query in queries:
                await db.execute(query)
