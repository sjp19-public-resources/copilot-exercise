import {
  NavLink,
  useNavigate,
} from "react-router-dom";

import useAuthService from "../authService";


function NavBar() {
  const { user, isLoaded, signout } = useAuthService();
  const navigate = useNavigate();

  const handleSignOut = async (e) => {
    e.preventDefault();
    await signout();
    navigate("/")
  };

  return (
    <div id="navbar">
      <div id="logo">WebThreads</div>
      {user && isLoaded ? (
        <div>
          <NavLink to="/shop-items">My Shop</NavLink>
          <NavLink to="/apparels">Apparels</NavLink>
          <a href="#" onClick={handleSignOut}>Sign out</a>
        </div>
      ) : null}
    </div>
  );
}

export default NavBar;
